echo "Build project into build directory"

ORIG_DIRECTORY=./src
BUILD_DIRECTORY=./build


#Recreate build directory
rm -rf $BUILD_DIRECTORY
mkdir $BUILD_DIRECTORY

#Process assets
cp -r $ORIG_DIRECTORY/assets $BUILD_DIRECTORY/assets

#Process pages
for i in $(ls $ORIG_DIRECTORY/pages);
do


	cat $ORIG_DIRECTORY/tpl/head.html $ORIG_DIRECTORY/pages/$i $ORIG_DIRECTORY/tpl/bottom.html > $BUILD_DIRECTORY/$i

	#Get the title of the page
	orig_title=$(cat $ORIG_DIRECTORY/tpl/head.html | grep -e "<title>");
	title="<title>"$(cat $ORIG_DIRECTORY/pages/$i | grep -e "data-title" | cut -d '"' -f 4)" - Grasshopper Shop</title>"

	title="${title//\//\\/}"
	orig_title="${orig_title//\//\\/}"

	sed -i -e "s/$orig_title/$title/g" $BUILD_DIRECTORY/$i

done


#Clean dirty directory
rm -rf .msys*
