#!/bin/bash

ORIG_DIRECTORY=./src
BUILD_DIRECTORY=./build
INFINITE_BUILD_DIRECTORY=./infinite_build
TEMP_DIRECTORY=./temp

while true
do
	mkdir -p $INFINITE_BUILD_DIRECTORY
	echo "Building infinitly. Press [CTRL+C] to stop.."
	bash ./scripts/build.sh

	#Avoid pages errors
	mv $INFINITE_BUILD_DIRECTORY $TEMP_DIRECTORY
	mv $BUILD_DIRECTORY $INFINITE_BUILD_DIRECTORY
	mv $TEMP_DIRECTORY $BUILD_DIRECTORY

	echo "Done. sleeping for a second..."
	sleep 1
done
