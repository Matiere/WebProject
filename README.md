# Web Project

This repository contains the project from Mathis and Pierre.

The website is live at : https://univ-projects.gitlab.io/WebProjectS1

Enjoy it! 

# To do list
* [x] Aligner les images sur la recherche
* [x] Champ de recherche / page de recherche accessible
* [x] W3C
* [x] Améliorer l'index
* [ ] Partenariat / pubs
* [x] Lien sur les images qui pointent vers des articles dans la page de resultats de recherche
* [x] Rajouter des resultats recherche
* [x] Rajouter un champ de recherche sur la page de résultats 

# Critères d'évaluation
* [x] Organisation des fichiers /1
* [x] Présence de toutes les pages demandées /2
* [x] Navigation entre les pages /1
* [x] Homogénéité et qualité de la charte graphique /1.5
* [x] Ergonomie /1
* [x] Originalité /0.5
* [x] Responsive /1.5
* [x] Orthographe /2
* [x] HTML propre /1 (indentation)
* [x] HTML sémantique /2 (pas de br, i, utilisation appropriée des classes)
* [x] Liens /1.5 (utilisation de liens relatifs quand c'est possible, présence de liens externes)
* [x] Formulaires /1
* [x] CSS propre /1
* [x] Richesse du CSS /1
* [x] Utilisation de GIT /1
* [x] Publication sur le web /1
