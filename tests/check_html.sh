#!/bin/bash

# Required softwares :
# * java -> openjdk-8-jre
# * wget
# * zip

# Get intrepreter
wget https://github.com/validator/validator/releases/download/18.8.29/vnu.jar_18.8.29.zip
unzip vnu.jar_18.8.29.zip
unzip -j vnu.jar_18.8.29.zip dist/vnu.jar

# Check project
java -jar vnu.jar --also-check-css build/*.html